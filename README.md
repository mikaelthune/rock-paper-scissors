# The game Rock Paper Scissors, played through a REST API

## Available endpoints
*:id* should be replaced with the id of the game you want 
### POST http://localhost:8080/api/game/init
Initialises a new game.  
Returns the id of the new game.

### POST http://localhost:8080/api/game/:id/play
Join an existing game and make your move.  
Payload should be a JSON with this format:  
{  
&nbsp; "name": "*Your name*",  
&nbsp; "move": "*Your move*"  
}
#### Allowed moves are (case sensitive):
ROCK  
PAPER  
SCISSORS

### GET http://localhost:8080/api/game/:id
Returns the status of the current game.

## How to play
- Initialise a new game (http://localhost:8080/api/game/init)
- Both players join the game with the id provided. Enter your name and move in the payload (http://localhost:8080/api/game/:id/play)
- Check the result of the game (http://localhost:8080/api/game/:id)

