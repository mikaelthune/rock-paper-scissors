package com.yay.rockpaperscissors.service;

import com.yay.rockpaperscissors.domain.Game;
import com.yay.rockpaperscissors.domain.GameStatus;
import com.yay.rockpaperscissors.domain.Player;
import com.yay.rockpaperscissors.exception.GameFullException;
import com.yay.rockpaperscissors.exception.GameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.UUID;

@Service
public class GameEngine
{
    private HashMap<String, Game> games;

    public GameEngine()
    {
        games = new HashMap<>();
    }

    public String initialiseNewGame()
    {
        Game game = new Game(generateGameId());
        games.put(game.getId(), game);
        return game.getId();
    }

    public void joinGame(String id, Player player) throws GameFullException, GameNotFoundException
    {
        Game game = findGame(id);
        game.addPlayer(player);
    }

    public String getGameStatus(String id) throws GameNotFoundException
    {
        Game game = findGame(id);

        if (game.getStatus() == GameStatus.READY_TO_RESOLVE)
        {
            game.resolve();
        }

        return createStatusString(game);
    }

    private Game findGame(String id) throws GameNotFoundException
    {
        Game game = games.get(id);
        if (game == null)
        {
            throw new GameNotFoundException();
        }
        else
        {
            return game;
        }
    }

    private String createStatusString(Game game)
    {
        return switch (game.getStatus())
                {
                    case WAITING_FOR_FIRST_PLAYER -> "Waiting for first player to join";
                    case WAITING_FOR_SECOND_PLAYER -> "Waiting for second player to join";
                    case READY_TO_RESOLVE -> ""; //Will never be reached
                    case TIE -> "The game is a tie";
                    case PLAYER1_WON -> {
                        Player player = game.getPlayer(0);
                        yield player.getName() + " Won the game with " + player.getMove();
                    }
                    case PLAYER2_WON -> {
                        Player player = game.getPlayer(1);
                        yield player.getName() + " Won the game with " + player.getMove();
                    }
                };
    }

    private String generateGameId()
    {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }
}
