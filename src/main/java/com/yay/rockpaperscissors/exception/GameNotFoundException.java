package com.yay.rockpaperscissors.exception;

public class GameNotFoundException extends Exception
{
    public GameNotFoundException()
    {
        super("Game not found");
    }
}
