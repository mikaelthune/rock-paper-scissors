package com.yay.rockpaperscissors.exception;

public class GameFullException extends Exception
{
    public GameFullException()
    {
        super("Game is full");
    }
}
