package com.yay.rockpaperscissors.controller;

import com.yay.rockpaperscissors.domain.Player;
import com.yay.rockpaperscissors.exception.GameFullException;
import com.yay.rockpaperscissors.exception.GameNotFoundException;
import com.yay.rockpaperscissors.service.GameEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping("/api/game")
public class ApiController
{
    @Autowired
    private GameEngine gameEngine;

    @PostMapping(path = "/init")
    public ResponseEntity<String> initialiseGame()
    {
        String id = gameEngine.initialiseNewGame();
        return new ResponseEntity<String>(id, HttpStatus.CREATED);
    }

    @PostMapping(path = "/{id}/play")
    public void playGame(@PathVariable String id, @RequestBody Player player)
    {
        try
        {
            gameEngine.joinGame(id, player);
        }
        catch (GameFullException e)
        {
            throw new ResponseStatusException(HttpStatus.LOCKED, "The game is full");
        }
        catch (GameNotFoundException e)
        {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "The game was not found");
        }
    }

    @GetMapping(path = "/{id}")
    public String getGameStatus(@PathVariable String id)
    {
        try
        {
            return gameEngine.getGameStatus(id);
        }
        catch (GameNotFoundException e)
        {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "The game was not found");
        }
    }
}
