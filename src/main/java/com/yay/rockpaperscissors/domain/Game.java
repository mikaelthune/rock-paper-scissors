package com.yay.rockpaperscissors.domain;

import com.yay.rockpaperscissors.exception.GameFullException;

import java.util.ArrayList;
import java.util.HashMap;

public class Game
{
    private final String id;
    private ArrayList<Player> players;
    private GameStatus status;

    private static final HashMap<PlayerMove, PlayerMove> rulesKeyWinsAgainstValue = new HashMap<>()
    {{
        put(PlayerMove.ROCK, PlayerMove.SCISSORS);
        put(PlayerMove.PAPER, PlayerMove.ROCK);
        put(PlayerMove.SCISSORS, PlayerMove.PAPER);
    }};

    public Game(String id)
    {
        this.id = id;
        players = new ArrayList<>();
        status = GameStatus.WAITING_FOR_FIRST_PLAYER;
    }

    public void addPlayer(Player player) throws GameFullException
    {
        if (players.size() == 2)
        {
            throw new GameFullException();
        }
        players.add(player);
        updateStatusAfterJoin();
    }

    private void updateStatusAfterJoin()
    {
        if (status == GameStatus.WAITING_FOR_FIRST_PLAYER)
        {
            status = GameStatus.WAITING_FOR_SECOND_PLAYER;
        }
        else
        {
            status = GameStatus.READY_TO_RESOLVE;
        }
    }

    public void resolve()
    {
        if (gameIsTie())
        {
            status = GameStatus.TIE;
        }
        else
        {
            status = determineWinner();
        }
    }

    private boolean gameIsTie()
    {
        return players.get(0).getMove() == players.get(1).getMove();
    }

    // Ties must be checked before
    private GameStatus determineWinner()
    {
        Player player1 = players.get(0);
        Player player2 = players.get(1);

        PlayerMove player1WouldWinAgainst = rulesKeyWinsAgainstValue.get(player1.getMove());
        if (player1WouldWinAgainst == player2.getMove())
        {
            return GameStatus.PLAYER1_WON;
        }
        else
        {
            return GameStatus.PLAYER2_WON;
        }
    }

    public String getId()
    {
        return id;
    }

    public GameStatus getStatus()
    {
        return status;
    }

    public Player getPlayer(int ind)
    {
        return players.get(ind);
    }
}
