package com.yay.rockpaperscissors.domain;

public enum GameStatus
{
    WAITING_FOR_FIRST_PLAYER,
    WAITING_FOR_SECOND_PLAYER,
    READY_TO_RESOLVE,
    PLAYER1_WON,
    PLAYER2_WON,
    TIE
}
