package com.yay.rockpaperscissors.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Player
{
    @JsonProperty
    private String name;
    @JsonProperty
    private PlayerMove move;

    public Player(String name, PlayerMove move)
    {
        this.name = name;
        this.move = move;
    }

    public String getName()
    {
        return name;
    }

    public PlayerMove getMove()
    {
        return move;
    }
}
