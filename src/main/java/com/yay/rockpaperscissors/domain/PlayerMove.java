package com.yay.rockpaperscissors.domain;

public enum PlayerMove
{
    ROCK,
    PAPER,
    SCISSORS
}
