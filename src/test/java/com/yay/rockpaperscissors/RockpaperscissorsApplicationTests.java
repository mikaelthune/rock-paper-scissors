package com.yay.rockpaperscissors;

import com.yay.rockpaperscissors.service.GameEngine;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.HashSet;

@SpringBootTest
class RockpaperscissorsApplicationTests
{
    GameEngine gameEngine;
    String id;

    @BeforeEach
    void setup()
    {
        gameEngine = new GameEngine();
    }

    @Test
    void contextLoads()
    {
    }

    @Test
    void createdGamesShouldHaveUniqueId()
    {
        ArrayList<String> idSet = new ArrayList<>();
        for (int i = 0; i < 10000; i++)
        {
            idSet.add(gameEngine.initialiseNewGame());
        }

        //Check for duplicates
        ArrayList<String> duplicates = new ArrayList<>();
        for (int i = 0; i < idSet.size() - 1; i++)
        {
            String id1 = idSet.get(i);
            for (int j = 1 + i; j < idSet.size(); j++)
            {
                String id2 = idSet.get(j);
                if (id1.equals(id2))
                {
                    duplicates.add(id2);
                }
            }
        }

        assertThat(duplicates, empty());
    }


}
