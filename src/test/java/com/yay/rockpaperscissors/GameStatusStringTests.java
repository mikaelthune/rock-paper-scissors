package com.yay.rockpaperscissors;

import com.yay.rockpaperscissors.domain.Player;
import com.yay.rockpaperscissors.domain.PlayerMove;
import com.yay.rockpaperscissors.exception.GameFullException;
import com.yay.rockpaperscissors.exception.GameNotFoundException;
import com.yay.rockpaperscissors.service.GameEngine;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.hamcrest.MatcherAssert.assertThat;

import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class GameStatusStringTests
{
    GameEngine gameEngine;
    String id;

    @BeforeEach
    void setup()
    {
        gameEngine = new GameEngine();
        id = gameEngine.initialiseNewGame();
    }

    @Test
    void newGameShouldBeWaitingForFirstPlayer() throws GameNotFoundException
    {
        String status = gameEngine.getGameStatus(id);
        assertEquals(status, "Waiting for first player to join");
    }

    @Test
    void gameShouldBeWaitingForSecondPlayerAfterJoin() throws GameNotFoundException, GameFullException
    {
        Player player = new Player("A", PlayerMove.PAPER);
        gameEngine.joinGame(id, player);

        String status = gameEngine.getGameStatus(id);

        assertEquals(status, "Waiting for second player to join");
    }

    @Test
    void gameShouldBeTiedIfPlayersChooseSameMove() throws GameNotFoundException, GameFullException
    {
        joinGameWithTwoPlayers("A", PlayerMove.PAPER, "B", PlayerMove.PAPER);

        String status = gameEngine.getGameStatus(id);

        assertEquals(status, "The game is a tie");
    }

    @Test
    void gameShouldReturnCorrectWinnerPaperRock() throws GameNotFoundException, GameFullException
    {
        joinGameWithTwoPlayers("A", PlayerMove.PAPER, "B", PlayerMove.ROCK);

        String status = gameEngine.getGameStatus(id);

        assertEquals(status, "A Won the game with PAPER");
    }

    @Test
    void gameShouldReturnCorrectWinnerRockScissors() throws GameNotFoundException, GameFullException
    {
        joinGameWithTwoPlayers("A", PlayerMove.SCISSORS, "B", PlayerMove.ROCK);

        String status = gameEngine.getGameStatus(id);

        assertEquals(status, "B Won the game with ROCK");
    }

    @Test
    void gameShouldReturnCorrectWinnerPaperScissors() throws GameNotFoundException, GameFullException
    {
        joinGameWithTwoPlayers("A", PlayerMove.SCISSORS, "B", PlayerMove.PAPER);

        String status = gameEngine.getGameStatus(id);

        assertEquals(status, "A Won the game with SCISSORS");
    }

    private void joinGameWithTwoPlayers(String name1, PlayerMove move1, String name2, PlayerMove move2) throws GameFullException, GameNotFoundException
    {
        Player playerA = new Player(name1, move1);
        Player playerB = new Player(name2, move2);

        gameEngine.joinGame(id, playerA);
        gameEngine.joinGame(id, playerB);
    }
}