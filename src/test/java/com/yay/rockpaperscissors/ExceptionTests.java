package com.yay.rockpaperscissors;

import com.yay.rockpaperscissors.domain.Player;
import com.yay.rockpaperscissors.domain.PlayerMove;
import com.yay.rockpaperscissors.exception.GameFullException;
import com.yay.rockpaperscissors.exception.GameNotFoundException;
import com.yay.rockpaperscissors.service.GameEngine;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ExceptionTests
{
    GameEngine gameEngine;
    String id;

    @BeforeEach
    void setup()
    {
        gameEngine = new GameEngine();
        id = gameEngine.initialiseNewGame();
    }

    @Test
    void shouldRequireValidIdToJoin() throws GameFullException
    {
        assertThrows(GameNotFoundException.class, () -> {
            gameEngine.joinGame("", new Player("A", PlayerMove.PAPER));
        });
    }

    @Test
    void shouldNotBeAbleToJoinFullGame() throws GameNotFoundException, GameFullException
    {
        gameEngine.joinGame(id, new Player("A", PlayerMove.PAPER));
        gameEngine.joinGame(id, new Player("B", PlayerMove.SCISSORS));

        assertThrows(GameFullException.class, () -> {
            gameEngine.joinGame(id, new Player("C", PlayerMove.SCISSORS));
        });
    }
}
